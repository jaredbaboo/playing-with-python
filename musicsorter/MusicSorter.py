#!/usr/bin/python
import os
import mutagen
import argparse
import shutil
filetypes =['.mp3','.flac','.aac','.wma','.ogg','.m4a',]

parser = argparse.ArgumentParser(prog='MusicSorter')

parser.add_argument('-s','--src',help='source to look for music files')
parser.add_argument('-d','--dest',help='destination where folders and files will be moved')
args = parser.parse_args()


source = args.src
destination = args.dest
'''
check that the required direcoty exists and create it if it doesn't
'''
def ensure_dir(directory):
	if not os.path.exists(directory):
		os.makedirs(directory)
'''
Extract required audio detail from music file
'''
def getAudioDetail(filename):
	artist='unknown'
	album='unknown'
	title='unknown'
	try:
		audio = mutagen.File(filename,options=None,easy=True)
		artist = audio.get('artist')
		album = audio.get('album') 
		title =audio.get('title') 
	except (mutagen.mp3.HeaderNotFoundError,mutagen.flac.FLACNoHeaderError):
		print 'Error processing file :',filename,'\r\n'
	return artist,title,album
'''
	format strings as required set default to unknown and remove unneeded characters
'''
def formatString(string):
	if string is None:
		string ='unknown'
	if not isinstance(string, basestring):
		string = string[0]
	string= string.replace('/',' ')
	return string.strip()	
'''
ensure destination file is unique adding a counter if not so
'''
def checkDestFile(artist,album,title,ext):
	fileDest = destination+'/'+artist+'/'+album
	ensure_dir(fileDest)
	duplicateCount = 0;
	while os.path.isfile(fileDest+'/'+title+'.'+ext):
		title = str(duplicateCount)+'_'+title	
		duplicateCount=duplicateCount+1
	return fileDest+'/'+title+ext

for dirname, dirnames, filenames in os.walk(source):
	for fileitem in filenames:
		print dirname+'/'+fileitem ,'\r\n'
		fileName = os.path.splitext(fileitem)
		if fileName[1] in filetypes:
		
			artist,title,album = getAudioDetail(dirname+'/'+fileitem)
			artist = formatString(artist)
			title = formatString(title)
			album = formatString(album)
			fileName = checkDestFile(artist,album,title,fileName[1])


			print fileName,'\r\n'
			shutil.move(dirname+'/'+fileitem,fileName)


				

